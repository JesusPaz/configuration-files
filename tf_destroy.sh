#!/bin/bash

# Use in the root of the repo

cd terraform/applications/account-api
terraform destroy -auto-approve
cd ../../infrastructure/account-api-infra
terraform destroy -auto-approve
cd ../ecs-cluster
terraform destroy -auto-approve
