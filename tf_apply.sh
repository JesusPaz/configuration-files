#!/bin/bash

# Use in the root of the repo

cd terraform/infrastructure/ecs-cluster
terraform init
terraform apply -auto-approve
cd ../account-api-infra
terraform init
terraform apply -auto-approve
cd ../../applications/account-api
terraform init
terraform apply -auto-approve
