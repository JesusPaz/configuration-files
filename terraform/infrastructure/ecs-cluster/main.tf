############
# S3 Backend
############

terraform {
  backend "s3" {
    bucket = "terraform-states-jp"
    key    = "dev-ecs-cluster.tfstate"
    region = "us-east-1"
  }
}

######################
# Deployment variables
######################

module "ecs_cluster" {
  source      = "s3::https://s3.amazonaws.com/terraform-resources-jp/ecs-cluster-0.1.2.zip"
  environment = "dev"
}
