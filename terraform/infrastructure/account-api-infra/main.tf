############
# S3 Backend
############

terraform {
  backend "s3" {
    bucket = "terraform-states-jp"
    key    = "dev-account-api-infra.tfstate"
    region = "us-east-1"
  }
}

######################
# Deployment variables
######################

module "account_api_infra" {
  source      = "s3::https://s3.amazonaws.com/terraform-resources-jp/account-api-infra-0.1.1.zip"
  environment = "dev"
}
