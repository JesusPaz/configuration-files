############
# S3 Backend
############

terraform {
  backend "s3" {
    bucket = "terraform-states-jp"
    key    = "dev-account-api.tfstate"
    region = "us-east-1"
  }
}

######################
# Deployment variables
######################

module "account_api" {
  source        = "s3::https://s3.amazonaws.com/terraform-resources-jp/account-api-0.1.2.zip"
  environment   = "dev"
  image_version = "develop"
}
