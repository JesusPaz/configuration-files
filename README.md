# Configuration files repository

## Notes

* This repository uses modules from https://gitlab.com/JesusPaz/terraform.
* Each branch corresponds to an environment, in this case there is only one environment (develop) but the modules in terraform are designed to be deployed in different environments.
* In order to run terraform you must have access to two s3 buckets, one for the states and one for the modules.

The order of executing the configurations is:
1. terraform/infrastructure/ecs-cluster
2. terraform/infrastructure/account-api-infra
3. terraform/applications/account-api

It must be executed in each path:
```
    terraform init
    terraform apply 
```
